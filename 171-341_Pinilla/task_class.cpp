#include "stdafx.h"
#include "task_class.h"
#include "iostream"
#include "string"

using namespace std;

task_class::task_class()
{
	text = new char[0];
}

task_class::task_class(char * txt)
{
	len = strlen(text);
	text = new char[len];
	text = txt;
}

task_class::task_class(task_class & txt)
{
	len = strlen(txt.text);
	text = new char[len];
	text = txt.text;
}

task_class::task_class(std::string & txt)
{
	len = txt.size();
	text = new char[len];
	for (int i = 0; i < len; i++)
	{
		text[i] = txt[i];
	}
}

void task_class::add(char *txt)
{
	copy_text = new char[len + strlen(txt) + 1];
	for (int i = 0; i < len; ++i)
		copy_text[i] = text[i];
	for (int k = 0; k < strlen(txt); ++k)
	copy_text[len + k] = txt[k];

	text = new char[len + strlen(txt)];

	for (int i = 0; i < len + strlen(txt); i++)
	{
		text[i] = copy_text[i];
	}
	delete[] copy_text;
}

void task_class::add(std::string & txt)
{
	copy_text = new char[len + txt.size() + 1];
	for (int i = 0; i < len; ++i)
		copy_text[i] = text[i];
	for (int k = 0; k < txt.size(); ++k)
		copy_text[len + k] = txt[k];

	text = new char[len + txt.size()];

	for (int i = 0; i < len + txt.size(); i++)
	{
		text[i] = copy_text[i];
	}
	delete[] copy_text;
	len += txt.size();
}

void task_class::insert(int local, char *txt)
{
	if ((local + strlen(txt)) <= (strlen(txt)))
	{
		for (int i = local; i < (local + strlen(txt)); i++)
		{
			text[i] = txt[i - local];
		}
	}
	else 
	{
		std::cout << "Wrong";
	}
}

void task_class::out()
{
	for (int i = 0; i < len; i++)
	{
		std::cout << text[i];
	}
}

void task_class::clear()
{
	len = 0;
	text = new char[len];
}

task_class::~task_class()
{
	delete[] text;
}
