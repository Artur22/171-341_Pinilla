#pragma once
#include "string"
class task_class
{
private:
	char *text;
	char *copy_text;
	int len = 0;
public:
	task_class();
	task_class(char *txt);
	task_class(task_class &txt);
	task_class(std::string &txt);
	void add(char *txt);
	void add(std::string &txt);

	void out();

	void insert(int local, char *txt);

	void clear();

	~task_class();
};

